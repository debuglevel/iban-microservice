package de.debuglevel.iban.bank

import io.micronaut.http.HttpResponse
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import mu.KotlinLogging
import java.util.*

@Controller("/banks")
class BankController(
    private val bankService: BankService,
) {
    private val logger = KotlinLogging.logger {}

    /**
     * Get all banks
     * @return All banks
     */
    @Get("/")
    fun getAllBanks(): HttpResponse<List<GetBankResponse>> {
        logger.debug("Called getAllBanks()")
        return try {
            val banks = bankService.getAll()
            val getBankResponses = banks
                .map { GetBankResponse(it) }

            HttpResponse.ok(getBankResponses)
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }

    /**
     * Get a bank
     * @param id ID of the bank
     * @return A bank
     */
    @Get("/{id}")
    fun getOneBank(id: UUID): HttpResponse<GetBankResponse> {
        logger.debug("Called getOneBank($id)")
        return try {
            val bank = bankService.get(id)

            val getBankResponse = GetBankResponse(bank)
            HttpResponse.ok(getBankResponse)
        } catch (e: BankService.ItemNotFoundException) {
            logger.debug { "Getting bank $id failed: ${e.message}" }
            HttpResponse.notFound()
        } catch (e: Exception) {
            logger.error(e) { "Unhandled exception" }
            HttpResponse.serverError()
        }
    }
}