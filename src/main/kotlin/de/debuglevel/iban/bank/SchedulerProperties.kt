package de.debuglevel.iban.bank

import io.micronaut.context.annotation.ConfigurationProperties
import java.time.Duration

/**
 * Downloading bank data from the data source is done in a scheduler
 */
@ConfigurationProperties("app.iban.datasource-scheduler")
class SchedulerProperties {
    /**
     * Wait this duration after startup to download the first time.
     */
    var initialDelay: Duration = Duration.ofSeconds(60)

    /**
     * Wait this duration after last schedule termination to download again.
     */
    var interval: Duration = Duration.ofDays(1)
}