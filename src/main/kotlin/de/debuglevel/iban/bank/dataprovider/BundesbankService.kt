package de.debuglevel.iban.bank.dataprovider

import de.debuglevel.iban.bank.Bank
import jakarta.inject.Singleton
import mu.KotlinLogging
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Workbook
import org.apache.poi.ss.usermodel.WorkbookFactory
import org.jsoup.Jsoup
import java.io.InputStream
import java.net.URL

@Singleton
class BundesbankService : BankDataProvider {
    private val logger = KotlinLogging.logger {}

    override val dataSourceIdentifier = "Bundesbank"
    override val countryCode = "DE"

    private val pageUrl =
        "https://www.bundesbank.de/de/aufgaben/unbarer-zahlungsverkehr/serviceangebot/bankleitzahlen/download-bankleitzahlen-602592"

    private val firstDataRow = 2

    private val headers = listOf(
        "Bank-leitzahl",
        "Merkmal",
        "Bezeichnung",
        "PLZ",
        "Ort",
        "Kurzbezeichnung",
        "PAN",
        "BIC",
        "Prüfziffer-berechnungs-methode",
        "Datensatz-nummer",
        "Änderungs-kennzeichen",
        "Bankleitzahl-löschung",
        "Nachfolge-Bankleitzahl"
    )

    private fun getInputStream(): InputStream {
        logger.debug { "Getting XLSX input..." }

        val url = extractXlsxUrl()

        return URL(url).openStream()
    }

    private fun extractXlsxUrl(): String {
        logger.debug { "Extracting XLSX URL..." }

        val url = pageUrl
        val document = Jsoup.connect(url).get()

        // get a tags with href ending with ".xlsx"
        val excelElements = document.select("a[href$=blz-aktuell-xlsx-data.xlsx]")
        logger.trace { "Found ${excelElements.size} matching elements." }
        if (excelElements.size != 1) {
            throw LinkNotFoundException(excelElements.size)
        }

        val xlsxElement = excelElements.first()
        val xlsxUrl = xlsxElement!!.attr("abs:href")

        logger.debug { "Extracted XLSX URL: $xlsxUrl" }
        return xlsxUrl
    }

    private fun parse(inputStream: InputStream): List<Bank> {
        logger.debug { "Parsing Excel workbook..." }

        logger.trace { "Opening Excel workbook..." }
        val workbook = WorkbookFactory.create(inputStream)
        logger.trace { "Opened Excel workbook" }

        validateFileFormat(workbook)

        // get the first sheet
        val sheet = workbook.getSheetAt(0)

        // get actual data rows; i.e. skip header row
        val dataRows = sheet.drop(firstDataRow - 1)

        logger.trace { "Converting rows to banks..." }
        val banks = dataRows.mapNotNull { row ->
            convert(row)
        }

        return banks
    }

    private fun convert(row: Row): Bank? {
        val nationalBankCode = row.getCell(0).stringCellValue
        val name = row.getCell(2).stringCellValue
        val shortName = row.getCell(5).stringCellValue
        val bic = row.getCell(7).stringCellValue
        val externalId = row.getCell(9).stringCellValue

        return if (externalId.isBlank()) {
            // HACK: if externalId is blank, assume it not to be a valid data row
            null
        } else {
            Bank(
                id = null,
                name = name,
                shortName = shortName,
                countryCode = "DE",
                nationalBankCode = nationalBankCode,
                businessIdentifierCode = bic,
                externalId = externalId,
                dataSource = dataSourceIdentifier
            )
        }
    }

    /**
     * Validate that the Excel sheet looks like what we expected during development.
     */
    private fun validateFileFormat(workbook: Workbook) {
        logger.debug { "Validating Excel workbook..." }

        val expectedSheetCount = 1
        logger.trace { "Validating number of sheets (expecting $expectedSheetCount)..." }
        if (workbook.numberOfSheets != expectedSheetCount) {
            throw BankDataProvider.UnexpectedFileFormatException("$expectedSheetCount sheets were expected; actually ${workbook.numberOfSheets}")
        }

        val expectedSheetName = "Daten"
        logger.trace { "Validating sheet name (expecting '$expectedSheetName')..." }
        val sheet = workbook.getSheetAt(0)
        if (sheet.sheetName != expectedSheetName) {
            throw BankDataProvider.UnexpectedFileFormatException("Expected sheet name '$expectedSheetName'; actually ${sheet.sheetName}")
        }

        logger.trace { "Validating column header..." }
        val headerRow = sheet.first()
        for (cell in headerRow) {
            logger.trace { "Header cell (address: ${cell.address}, index: ${cell.columnIndex}) has value '${cell.stringCellValue}'" }
        }

        val expectedColumnCount = headers.size
        logger.trace { "Validating column count (expecting $expectedColumnCount)..." }
        // CAVEAT: do not use Row.lastCellNum() as it is counter-intuitive (last cell plus one)
        val columnsCount = headerRow.toList().count()
        logger.trace { "Found $columnsCount columns..." }
        if (columnsCount != expectedColumnCount) {
            throw BankDataProvider.UnexpectedFileFormatException("Expected column count to be $expectedColumnCount; actually $columnsCount (first cell: ${headerRow.firstCellNum.toInt()}, last cell: ${headerRow.lastCellNum.toInt()})")
        }

        for (header in headers.withIndex()) {
            logger.trace { "Validating column ${header.index} value to be '${header.value}'..." }

            val actualHeader = headerRow.elementAt(header.index).stringCellValue
            if (actualHeader != header.value) {
                throw BankDataProvider.UnexpectedFileFormatException("Expected column ${header.index} value '${header.value}'; actually '$actualHeader'")
            }
        }

        logger.debug { "Validated Excel workbook" }
    }

    override fun getAll(): List<Bank> {
        logger.debug { "Getting banks..." }

        val inputStream = getInputStream()
        val banks = parse(inputStream)

        logger.debug { "Got ${banks.size} banks..." }
        return banks
    }

    class LinkNotFoundException(occurrences: Int) : Exception("Did not find exactly 1 '.xlsx' link but $occurrences")
}

