package de.debuglevel.iban.bank.dataprovider

import de.debuglevel.iban.bank.Bank
import mu.KotlinLogging
import java.io.BufferedReader
import java.io.InputStream
import java.net.URL
import jakarta.inject.Singleton

@Singleton
class NationalbankService : BankDataProvider {
    private val logger = KotlinLogging.logger {}

    override val dataSourceIdentifier = "Nationalbank"
    override val countryCode = "AT"

    private val url = "https://www.oenb.at/docroot/downloads_observ/sepa-zv-vz_gesamt.csv"
    private val csvDelimiter = ";"

    private val headerRow = 6
    private val firstDataRow = 7

    private val nationalBankCodeColumn = 2
    private val nameColumn = 6
    private val bicColumn = 18
    private val identnummerColumn = 1
    private val firmenbuchnummerColumn = 5
    private val leiColumn = 21

    private val headers: List<String> = listOf(
        "Kennzeichen",
        "Identnummer",
        "Bankleitzahl",
        "Institutsart",
        "Sektor",
        "Firmenbuchnummer",
        "Bankenname",
        "Straße",
        "PLZ",
        "Ort",
        "Postadresse / Straße",
        "Postadresse / PLZ",
        "Postadresse / Ort",
        "Postfach",
        "Bundesland",
        "Telefon",
        "Fax",
        "E-Mail",
        "SWIFT-Code",
        "Homepage",
        "Gründungsdatum",
        "LEI",
    )

    private fun getInputStream(): InputStream {
        logger.debug { "Getting CSV input..." }
        return URL(url).openStream()
    }

    private fun parse(inputStream: InputStream): List<Bank> {
        logger.debug { "Parsing CSV..." }

        logger.trace { "Reading InputStream..." }
        val csv = inputStream.bufferedReader(charset = Charsets.ISO_8859_1).use(BufferedReader::readText)

        validateFileFormat(csv)

        // get actual data rows; i.e. skip header rows, skip blank lines
        val dataLines = csv.lines()
            .drop(firstDataRow - 1)
            .filterNot { it.isBlank() }

        logger.trace { "Converting CSV rows to banks..." }
        val banks = dataLines.mapNotNull { line -> convert(line) }

        logger.debug { "Parsed CSV into ${banks.size} banks." }
        return banks
    }

    private fun convert(line: String): Bank? {
        logger.trace { "Converting CSV line to bank..." }

        val columns = line.split(csvDelimiter)

        val nationalBankCode = columns[nationalBankCodeColumn]
        val name = columns[nameColumn]
        val shortName = columns[nameColumn]
        val bic = columns[bicColumn]
        val externalId = buildCompositeExternalId(columns) // there is no identifier; build our own.

        return if (externalId.isBlank()) {
            // HACK: if externalId is blank, assume it not to be a valid data row
            null
        } else {
            Bank(
                id = null,
                name = name,
                shortName = shortName,
                countryCode = countryCode,
                nationalBankCode = nationalBankCode,
                businessIdentifierCode = bic,
                externalId = externalId,
                dataSource = dataSourceIdentifier
            )
        }
    }

    private fun buildCompositeExternalId(columns: List<String>): String {
        return listOf(
            columns[identnummerColumn],
            columns[nationalBankCodeColumn],
            columns[firmenbuchnummerColumn],
            columns[bicColumn],
            columns[leiColumn]
        )
            .joinToString("-")
    }

    /**
     * Validate that the CSV file looks like what we expected during development.
     */
    private fun validateFileFormat(csv: String) {
        logger.debug { "Validating CSV..." }

        logger.trace { "Validating column header..." }
        val headerRow = csv.lines().drop(headerRow - 1).first()

        val headerCells = headerRow.split(csvDelimiter)
        for (cell in headerCells) {
            logger.trace { "Header cell has value '${cell}'" }
        }

        val expectedColumnCount = headers.size
        logger.trace { "Validating column count (expecting $expectedColumnCount)..." }
        val columnsCount = headerCells.count()
        logger.trace { "Found $columnsCount columns..." }
        if (columnsCount != expectedColumnCount) {
            throw BankDataProvider.UnexpectedFileFormatException("Expected column count to be $expectedColumnCount; actually $columnsCount")
        }

        for (header in headers.withIndex()) {
            logger.trace { "Validating column ${header.index} value to be '${header.value}'..." }

            val actualHeader = headerCells.elementAt(header.index)
            if (actualHeader != header.value) {
                throw BankDataProvider.UnexpectedFileFormatException("Expected column ${header.index} value '${header.value}'; actually $actualHeader")
            }
        }

        logger.debug { "Validated CSV" }
    }

    override fun getAll(): List<Bank> {
        logger.debug { "Getting banks..." }

        val inputStream = getInputStream()
        val banks = parse(inputStream)

        logger.debug { "Got ${banks.size} banks..." }
        return banks
    }
}

