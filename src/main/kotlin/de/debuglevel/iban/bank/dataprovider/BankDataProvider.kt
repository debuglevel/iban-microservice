package de.debuglevel.iban.bank.dataprovider

import de.debuglevel.iban.bank.Bank

interface BankDataProvider {
    /**
     * ID of the data source
     */
    val dataSourceIdentifier: String

    /**
     * Code of the country this provider delivers data for
     */
    val countryCode: String

    /**
     * Retrieve all banks
     */
    fun getAll(): List<Bank>

    /**
     * Exception thrown when the file does not have the expected format.
     */
    class UnexpectedFileFormatException(msg: String) : Exception(msg)
}