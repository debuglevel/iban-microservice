package de.debuglevel.iban.bank

import io.micronaut.data.annotation.Repository
import io.micronaut.data.repository.CrudRepository
import java.util.*

@Repository
interface BankRepository : CrudRepository<Bank, UUID>