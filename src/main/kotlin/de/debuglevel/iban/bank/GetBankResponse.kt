package de.debuglevel.iban.bank

import java.time.LocalDateTime
import java.util.*

data class GetBankResponse(
    /**
     * ID
     */
    val id: UUID?,
    /**
     * Name of the bank
     */
    val name: String,
    /**
     * Short or shorted name of the bank
     */
    val shortName: String,
    /**
     * Country code (although it should also be encoded in the BIC)
     */
    val countryCode: String,
    /**
     * National bank code
     */
    val nationalBankCode: String,
    /**
     * The Business Identifier Code (BIC)
     */
    val businessIdentifierCode: String,
    /**
     * Upstream ID from the data source.
     */
    val externalId: String,
    /**
     * Data source
     */
    val dataSource: String,
    /**
     * DateTime when the item was created.
     */
    val createdOn: LocalDateTime,
    /**
     * DateTime when the item was updated.
     */
    var modifiedOn: LocalDateTime,
) {
    constructor(bank: Bank) : this(
        bank.id,
        bank.name,
        bank.shortName,
        bank.countryCode,
        bank.nationalBankCode,
        bank.businessIdentifierCode,
        bank.externalId,
        bank.dataSource,
        bank.createdOn,
        bank.modifiedOn,
    )
}
