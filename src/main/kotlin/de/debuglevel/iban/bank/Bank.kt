package de.debuglevel.iban.bank

import io.micronaut.data.annotation.DateCreated
import io.micronaut.data.annotation.DateUpdated
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Bank(
    @Id
    @GeneratedValue
    var id: UUID?,
    /**
     * Name of the bank
     */
    var name: String,
    /**
     * Short or shorted name of the bank
     */
    var shortName: String,
    /**
     * Country code (although it should also be encoded in the BIC)
     */
    var countryCode: String,
    /**
     * National bank code
     */
    var nationalBankCode: String,
    /**
     * The Business Identifier Code (BIC)
     */
    var businessIdentifierCode: String,
    /**
     * Upstream ID from the data source
     */
    var externalId: String,
    /**
     * Data source
     */
    var dataSource: String,
    /**
     * DateTime when the item was created.
     */
    @DateCreated
    var createdOn: LocalDateTime = LocalDateTime.now(),
    /**
     * DateTime when the item was updated.
     */
    @DateUpdated
    var modifiedOn: LocalDateTime = LocalDateTime.now()
)