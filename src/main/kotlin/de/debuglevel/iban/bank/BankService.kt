package de.debuglevel.iban.bank

import de.debuglevel.iban.bank.dataprovider.BankDataProvider
import de.debuglevel.iban.bank.dataprovider.BundesbankService
import de.debuglevel.iban.bank.dataprovider.NationalbankService
import io.micronaut.scheduling.annotation.Scheduled
import mu.KotlinLogging
import java.util.*
import jakarta.inject.Singleton
import kotlin.time.ExperimentalTime

@Singleton
class BankService(
    private val bankRepository: BankRepository,
    private val bundesbankService: BundesbankService,
    private val nationalbankService: NationalbankService,
) {
    private val logger = KotlinLogging.logger {}

    private val bankDataProviders = listOf(bundesbankService, nationalbankService)

    val count: Long
        get() {
            logger.debug { "Getting banks count..." }

            val count = bankRepository.count()

            logger.debug { "Got banks count: $count" }
            return count
        }

    fun exists(id: UUID): Boolean {
        logger.debug { "Checking if bank $id exists..." }

        val isExisting = bankRepository.existsById(id)

        logger.debug { "Checked if bank $id exists: $isExisting" }
        return isExisting
    }

    fun get(id: UUID): Bank {
        logger.debug { "Getting bank with ID '$id'..." }

        val bank: Bank = bankRepository.findById(id).orElseThrow {
            logger.debug { "Getting bank with ID '$id' failed" }
            ItemNotFoundException(id)
        }

        logger.debug { "Got bank with ID '$id': $bank" }
        return bank
    }

    fun getAll(): Set<Bank> {
        logger.debug { "Getting all banks ..." }

        val banks = bankRepository.findAll().toSet()

        logger.debug { "Got ${banks.size} banks" }
        return banks
    }

    fun add(bank: Bank): Bank {
        logger.debug { "Adding bank '$bank'..." }

        val savedBank = bankRepository.save(bank)

        logger.debug { "Added bank: $savedBank" }
        return savedBank
    }

    fun update(id: UUID, bank: Bank): Bank {
        logger.debug { "Updating bank '$bank' with ID '$id'..." }

        // an object must be known to Hibernate (i.e. retrieved first) to get updated;
        // it would be a "detached entity" otherwise.
        val updateBank = this.get(id).apply {
            name = bank.name
            shortName = bank.shortName
            countryCode = bank.countryCode
            nationalBankCode = bank.nationalBankCode
            businessIdentifierCode = bank.businessIdentifierCode
            externalId = bank.externalId
            dataSource = bank.dataSource
        }

        val updatedBank = bankRepository.update(updateBank)

        logger.debug { "Updated bank: $updatedBank with ID '$id'" }
        return updatedBank
    }

    fun delete(id: UUID) {
        logger.debug { "Deleting bank with ID '$id'..." }

        if (bankRepository.existsById(id)) {
            bankRepository.deleteById(id)
        } else {
            throw ItemNotFoundException(id)
        }

        logger.debug { "Deleted bank with ID '$id'" }
    }

    fun deleteAll() {
        logger.debug { "Deleting all banks..." }

        val countBefore = bankRepository.count()
        bankRepository.deleteAll() // CAVEAT: does not delete dependent entities; use this instead: bankRepository.findAll().forEach { bankRepository.delete(it) }
        val countAfter = bankRepository.count()
        val countDeleted = countBefore - countAfter

        logger.debug { "Deleted $countDeleted of $countBefore banks, $countAfter remaining" }
    }

    @ExperimentalTime
    @Scheduled(
        fixedDelay = "\${app.iban.datasource-scheduler.interval:1d}",
        initialDelay = "\${app.iban.datasource-scheduler.initial-delay:60s}"
    )
    fun updateBanks() {
        logger.debug { "Periodically updating banks from data providers..." }

        for (bankDataProvider in bankDataProviders) {
            updateBanks(bankDataProvider)
        }

        logger.debug { "Periodically updated banks from data providers" }
    }

    private fun updateBanks(bankDataProvider: BankDataProvider) {
        logger.debug { "Updating ${bankDataProvider.countryCode} banks from ${bankDataProvider.dataSourceIdentifier}..." }

        val localBanks = getAll()
            .filter { it.countryCode == bankDataProvider.countryCode }

        val upstreamBanks = bankDataProvider.getAll()

        logger.debug { "Updating local banks..." }
        val updateBanks = upstreamBanks
            // get upstream banks which already exist locally
            .filter { upstreamBank ->
                localBanks.any { localBank ->
                    localBank.externalId == upstreamBank.externalId
                }
            }
            // add local ID to upstream bank
            .map { upstreamBank ->
                upstreamBank.copy(id = localBanks.find { localBank ->
                    localBank.externalId == upstreamBank.externalId
                }!!.id)
            }
        updateBanks.forEach { update(it.id!!, it) }
        logger.debug { "Updated ${updateBanks.size} local banks" }

        logger.debug { "Adding upstream-only banks..." }
        val onlyUpstreamBanks = upstreamBanks
            // get upstream banks which do not exist locally
            .filter { upstreamBank ->
                localBanks.none { localBank ->
                    localBank.externalId == upstreamBank.externalId
                }
            }
        onlyUpstreamBanks.forEach { add(it) }
        logger.debug { "Added ${onlyUpstreamBanks.size} upstream-only banks" }

        logger.debug { "Deleting local-only banks..." }
        val onlyLocalBanks = localBanks
            // get local banks which do not exist upstream
            .filter { localBank ->
                upstreamBanks.none { upstreamBank ->
                    upstreamBank.externalId == localBank.externalId
                }
            }
        onlyLocalBanks.forEach { delete(it.id!!) }
        logger.debug { "Deleted ${onlyLocalBanks.size} local-only banks" }

        logger.debug { "Updated ${bankDataProvider.countryCode} banks from ${bankDataProvider.dataSourceIdentifier}" }
    }

    class ItemNotFoundException(criteria: Any) : Exception("Item '$criteria' does not exist.")
}